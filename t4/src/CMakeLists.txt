set(src_list "main.c")
add_executable(main ${src_list})

include_directories(/home/luozikuan/work/CMakePractice/t3/usr/include/hello)
# INCLUDE_DIRECTORIES([AFTER|BEFORE] [SYSTEM] dir1 dir2 ...)
# 这条指令可以用来向工程添加多个特定的头文件搜索路径,路径之间用空格分
# 割,如果路径中包含了空格,可以使用双引号将它括起来,默认的行为是追加到当
# 前的头文件搜索路径的后面,你可以通过两种方式来进行控制搜索路径添加的方
# 式:
# 1,CMAKE_INCLUDE_DIRECTORIES_BEFORE,通过 SET 这个 cmake 变量为 on,可以
# 将添加的头文件搜索路径放在已有路径的前面。
# 2,通过 AFTER 或者 BEFORE 参数,也可以控制是追加还是置前。

link_directories(/home/luozikuan/work/CMakePractice/t3/usr/lib)
target_link_libraries(main hello)
# target_link_libraries(main libhello.a) #链接静态库